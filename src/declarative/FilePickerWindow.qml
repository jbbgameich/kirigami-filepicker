// SPDX-FileCopyrightText: 2019-2020 Jonah Brüchert <jbb@kaidan.im>
// SPDX-FileCopyrightText: 2019 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: GPL-2.0-or-later

import QtQuick 2.7
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.5 as Kirigami
import org.kde.kirigamifilepicker 0.1

/**
 * The FilePickerWindow type is used by the c++ MobileFileDialog class.
 * It should not be used from QML,
 * its only purpose is to create an integration with c++ using its FileChooserCallback.
 */
Kirigami.ApplicationWindow {
    id: root
    title: callback.title
    visible: false

    globalDrawer: PlacesGlobalDrawer {
        onPlaceOpenRequested: {
            filePicker.folder = place;
            close()
        }
    }

    onClosing: (close) => {
        close.accepted = false
        callback.cancel()
        close.accepted = true
    }

    FileChooserCallback {
        id: callback
        objectName: "callback"

        Component.onCompleted: console.log(JSON.stringify(callback))
    }

    Connections {
        target: filePicker

        onAccepted: (urls) => {
            console.log("### Accepted")
            callback.accepted(urls)
        }
    }


    pageStack.initialPage: FilePicker {
        id: filePicker
        selectMultiple: callback.selectMultiple
        selectExisting: callback.selectExisting
        nameFilters: callback.nameFilters
        mimeTypeFilters: callback.mimeTypeFilters
        currentFile: callback.currentFile
        acceptLabel: callback.acceptLabel
        selectFolder: callback.selectFolder
        Component.onCompleted: {
            // set conditional properties
            if (callback.folder) {
                console.log("Initial folder set")
                filePicker.folder = callback.folder
            } else {
                console.log("Initial folder not set")
            }

            if (callback.title) {
                console.log("Custom title set")
                filePicker.title = callback.title
            } else {
                console.log("Custom title not set")
            }
        }
    }
}
