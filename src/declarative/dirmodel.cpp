// SPDX-FileCopyrightText: 2019 Linus Jahn <lnj@kaidan.im>
// SPDX-FileCopyrightText: 2019-2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include "dirmodel.h"

#include <KIOCore/KCoreDirLister>
#include <QDebug>
#include <QUrl>

DirModel::DirModel(QObject *parent)
    : QAbstractListModel(parent), m_lister(new KCoreDirLister(this)), m_showDotFiles(m_lister->showingDotFiles()), m_isLoading(false)
{
    connect(m_lister, qOverload<>(&KCoreDirLister::completed), this, &DirModel::handleCompleted);
    connect(m_lister, &KCoreDirLister::newItems, this, &DirModel::handleNewItems);
    connect(m_lister, &KCoreDirLister::itemsDeleted, this, &DirModel::handleItemsDeleted);
    connect(m_lister, qOverload<const QUrl &, const QUrl &>(&KCoreDirLister::redirection), this, &DirModel::handleRedirection);
}

int DirModel::rowCount(const QModelIndex &) const
{
    return m_items.size();
}

QVariant DirModel::data(const QModelIndex &index, int role) const
{
    if (!hasIndex(index.row(), index.column(), index.parent()))
        return {};

    switch (role) {
    case Name:
        return m_items.at(index.row()).name();
    case Url:
        return m_items.at(index.row()).url();
    case IconName:
        return m_items.at(index.row()).iconName();
    case IsDir:
        return m_items.at(index.row()).isDir();
    case IsLink:
        return m_items.at(index.row()).isLink();
    case FileSize:
        return m_items.at(index.row()).size();
    case MimeType:
        return m_items.at(index.row()).mimetype();
    case IsHidden:
        return m_items.at(index.row()).isHidden();
    case IsReadable:
        return m_items.at(index.row()).isReadable();
    case IsWritable:
        return m_items.at(index.row()).isWritable();
    case ModificationTime:
        return m_items.at(index.row()).time(KFileItem::ModificationTime);
    default:
        return {};
    }
}

QHash<int, QByteArray> DirModel::roleNames() const
{
    return {
        {Name, QByteArrayLiteral("name")},
        {Url, QByteArrayLiteral("url")},
        {IconName, QByteArrayLiteral("iconName")},
        {IsDir, QByteArrayLiteral("isDir")},
        {IsLink, QByteArrayLiteral("isLink")},
        {FileSize, QByteArrayLiteral("fileSize")},
        {MimeType, QByteArrayLiteral("mimeType")},
        {IsHidden, QByteArrayLiteral("isHidden")},
        {IsReadable, QByteArrayLiteral("isReadable")},
        {IsWritable, QByteArrayLiteral("isWritable")},
        {ModificationTime, QByteArrayLiteral("modificationTime")}
    };
}

QUrl DirModel::folder() const
{
    return m_folder;
}

void DirModel::setFolder(const QUrl &folder)
{
    if (folder != m_folder) {
        beginResetModel();
        m_items.clear();
        endResetModel();

        setIsLoading(true);
        m_lister->openUrl(folder);
        // Set m_folder to where we actually are, not where we wanted to be
        m_folder = m_lister->url();

        emit folderChanged();
    }
}

void DirModel::handleCompleted()
{
    setIsLoading(false);
}

void DirModel::handleNewItems(const KFileItemList &items)
{
    beginInsertRows({}, rowCount(), rowCount() + items.size() - 1);

    for (const auto &item : items)
        m_items << item;

    endInsertRows();
}

void DirModel::handleItemsDeleted(const KFileItemList &items)
{
    qDebug() << "handleItemsDeletede" << items.size();
    int i = 0;
    QMutableVectorIterator<KFileItem> itr(m_items);
    while (itr.hasNext()) {
        if (items.contains(itr.next())) {
            beginRemoveRows({}, i, i);
            itr.remove();
            endRemoveRows();
        } else {
            i++;
        }
    }
}

void DirModel::handleRedirection(const QUrl &, const QUrl &newUrl)
{
    m_folder = newUrl;
    emit folderChanged();
}

bool DirModel::isLoading() const
{
    return m_isLoading;
}

void DirModel::setIsLoading(bool isLoading)
{
    if (m_isLoading != isLoading) {
        m_isLoading = isLoading;
        emit isLoadingChanged();
    }
}

bool DirModel::showDotFiles() const
{
    return m_showDotFiles;
}

void DirModel::setShowDotFiles(bool showDotFiles)
{
    if (showDotFiles != m_showDotFiles) {
        m_showDotFiles = showDotFiles;

        m_lister->setShowingDotFiles(showDotFiles);
        m_lister->emitChanges();

        emit showDotFilesChanged();
    }
}

QString DirModel::nameFilter() const
{
    return m_nameFilter;
}

void DirModel::setNameFilter(const QString &nameFilter)
{
    if (nameFilter != m_nameFilter) {
        m_nameFilter = nameFilter;

        m_lister->setNameFilter(nameFilter);
        m_lister->emitChanges();

        emit nameFilterChanged();
    }
}

QStringList DirModel::mimeFilters() const
{
    return m_mimeFilters;
}

void DirModel::setMimeFilters(const QStringList &mimeFilters)
{
    if (mimeFilters != m_mimeFilters) {
        m_mimeFilters = mimeFilters;

        m_lister->setMimeFilter(mimeFilters);
        m_lister->emitChanges();

        emit mimeFiltersChanged();
    }
}
