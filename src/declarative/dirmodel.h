// SPDX-FileCopyrightText: 2019 Linus Jahn <lnj@kaidan.im>
// SPDX-FileCopyrightText: 2019-2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QAbstractListModel>
#include <QVariant>
#include <KIOCore/KFileItem>
class KCoreDirLister;
class KFileItemList;

class DirModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(const QUrl& folder READ folder WRITE setFolder NOTIFY folderChanged)
    Q_PROPERTY(bool showDotFiles READ showDotFiles WRITE setShowDotFiles NOTIFY showDotFilesChanged)
    Q_PROPERTY(bool isLoading READ isLoading NOTIFY isLoadingChanged)
    Q_PROPERTY(QString nameFilter READ nameFilter NOTIFY nameFilterChanged)
    Q_PROPERTY(QStringList mimeFilters READ mimeFilters NOTIFY mimeFiltersChanged)

public:
    enum Roles {
        Name = Qt::UserRole + 1,
        Url,
        IconName,
        IsDir,
        IsLink,
        FileSize,
        MimeType,
        IsHidden,
        IsReadable,
        IsWritable,
        ModificationTime
    };

    Q_ENUM(Roles)

    explicit DirModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QHash<int, QByteArray> roleNames() const override;

    QUrl folder() const;
    void setFolder(const QUrl &folder);

    bool showDotFiles() const;
    void setShowDotFiles(bool showDotFiles);

    bool isLoading() const;
    void setIsLoading(bool isLoading);

    QString nameFilter() const;
    void setNameFilter(const QString &nameFilter);

    QStringList mimeFilters() const;
    void setMimeFilters(const QStringList &mimeFilters);

Q_SIGNALS:
    void folderChanged();
    void showDotFilesChanged();
    void isLoadingChanged();
    void nameFilterChanged();
    void mimeFiltersChanged();

private Q_SLOTS:
    void handleCompleted();
    void handleNewItems(const KFileItemList &items);
    void handleItemsDeleted(const KFileItemList &items);
    void handleRedirection(const QUrl &oldUrl, const QUrl &newUrl);

private:
    KCoreDirLister *m_lister;
    QVector<KFileItem> m_items;

    QUrl m_folder;
    bool m_showDotFiles;
    bool m_isLoading;
    QString m_nameFilter;
    QStringList m_mimeFilters;
};
