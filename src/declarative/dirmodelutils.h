// SPDX-FileCopyrightText: 2019 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QObject>

class DirModelUtils : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString homePath READ homePath NOTIFY homePathChanged)

public:
    explicit DirModelUtils(QObject *parent = nullptr);

    Q_INVOKABLE QStringList getUrlParts(const QUrl &url) const;
    Q_INVOKABLE QUrl indexOfUrl(const QUrl &url, int index) const;
    Q_INVOKABLE QString homePath() const;

Q_SIGNALS:
    void homePathChanged();
};
