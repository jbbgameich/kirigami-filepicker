// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-2.0-or-later

import QtQuick 2.7
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2 as Controls
import org.kde.kirigami 2.5 as Kirigami

import org.kde.kirigamifilepicker 0.1

/**
 * The FilePicker type provides a file picker wrapped in a Kirigmi.Page.
 * It can be directly pushed to the pageStack.
 */
Kirigami.ScrollablePage {
    id: root

    property bool selectMultiple
    property bool selectExisting
    property var nameFilters: []
    property var mimeTypeFilters: []
    property alias folder: dirModel.folder
    property string currentFile
    property string acceptLabel
    property bool selectFolder

    Component.onCompleted: {
        // Reset to home path if the url is empty
        if (!dirModel.folder.toString()) {
            dirModel.folder = utils.homePath
        }
    }

    title: selectExisting ? i18n("Open File") : i18n("Save File")

    // result
    property url fileUrl
    property var fileUrls: []

    onFileUrlsChanged: print("root.fileUrls", root.fileUrls)
    onSelectFolder: print("existing:", root.selectExisting)

    signal accepted(var urls)

    function addOrRemoveUrl(url) {
        print("addOrRemoveUrl")
        var index = root.fileUrls.indexOf(url)
        if (index > -1) {
            print("pop")
            // remove element
            root.fileUrls.splice(index, 1)
        } else {
            print("push")
            root.fileUrls.push(url)
        }
        root.fileUrlsChanged()
    }

    header: Controls.ToolBar {
        Row {
            Controls.ToolButton {
                icon.name: "folder-root-symbolic"
                height: parent.height
                width: height
                onClicked: dirModel.folder = "file:///"
            }

            Repeater {
                model: utils.getUrlParts(dirModel.folder)

                Controls.ToolButton {
                    icon.name: "arrow-right"
                    text: modelData
                    onClicked: dirModel.folder = utils.indexOfUrl(
                                    dirModel.folder, index)
                }
            }
        }
    }
    footer: RowLayout {
        visible: !root.selectExisting
        height: root.selectExisting ? 0 : Kirigami.Units.gridUnit * 2
        Controls.TextField {
            Layout.fillHeight: true
            Layout.fillWidth: true
            id: fileNameField
            placeholderText: i18n("File name")
        }
        Controls.ToolButton {
            Layout.fillHeight: true
            icon.name: "dialog-ok-apply"
            onClicked: {
                root.fileUrl = dirModel.folder + "/" + fileNameField.text
                root.accepted([root.fileUrl])
            }
        }
    }

    mainAction: Kirigami.Action {
        visible: (root.selectMultiple || root.selectFolder) && root.selectExisting
        text: root.acceptLabel ? root.acceptLabel : i18n("Select")
        icon.name: "object-select-symbolic"

        onTriggered: root.accepted(root.fileUrls)
    }

    DirModel {
        id: dirModel
        folder: root.folder
        showDotFiles: false
    }

    DirModelUtils {
        id: utils
    }

    Controls.BusyIndicator {
        anchors.centerIn: parent

        width: Kirigami.Units.gridUnit * 4
        height: width

        visible: dirModel.isLoading
    }

    ListView {
        anchors.fill: parent
        model: dirModel
        clip: true

        delegate: Kirigami.BasicListItem {
            text: model.name
            icon: model.iconName
            checkable: root.selectMultiple
            checked: root.fileUrls.includes(model.url)

            onClicked: {
                print("selectFolder", root.selectFolder)
                if (model.isDir) {
                    print("isDir")
                    if (root.selectFolder) {
                        if (root.selectMultiple) {
                            root.addOrRemoveUrl(model.url)
                        } else {
                            root.fileUrl = model.url
                        }
                    }

                    dirModel.folder = model.url
                } else {
                    if (!root.selectFolder) {
                        print("selectFolder is disabled")
                        if (root.selectMultiple) {
                            root.addOrRemoveUrl(model.url)
                        } else {
                            print("selectMultiple is disabled")
                            console.log("Accepted", "selectMultiple", root.selectMultiple)
                            root.fileUrl = model.url
                            root.fileUrls = [model.url]
                            root.accepted(root.fileUrls)
                        }
                    }
                }
            }
        }
    }
}
